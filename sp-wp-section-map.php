<?php
/**
 * Plugin Name: SP Wordpress Sektionskarte
 * Plugin URI: ch.sp-ps.sp-wp-section-map
 * Description: Wordpress-Plugin, um die Sektionskarte und -Liste darzustellen
 * Version: 1.2
 * Author: Sandro Covo, Andreas Weibel
 * Author URI: http://www.sp-ps.ch
 */
 
add_shortcode( 'sp_section_list', 'sp_section_list' );

function _display_section($section, $depth){

	$indent = '';
	for($i = 0; $i <= $depth; $i++){
		$indent .= '&nbsp;&nbsp;&nbsp;';
	}

	$ret = '<p class="sp_section_list sp_section_list_depth_' . $depth . '" >' . $indent;
	
	if(!empty($section['website'])){
		$ret .= "<a href='" . $section['website'] . "' target='_blank'>" . $section['name'] . "</a>";
	} else {
		$ret .= $section['name'];
	}
	
	$ret .= '</p>';
	
	if(!empty($section['children'])){
		foreach($section['children'] as $child){
			$ret .= _display_section($child, $depth + 1);
		}
		$ret .= '<hr/>';
	}

	return $ret;
}

function sp_section_list( $atts, $content, $tag ){

	wp_enqueue_style("sp-section-map-shortcode-jscss-style");
	
	if(empty($atts['canton'])){
		$atts['canton'] = '';
	}

	$result = wp_remote_post('https://tel.sp-ps.ch/sections/all_sections/' . $atts['canton']);
	
	if(is_wp_error($result) || empty ($result['body'])){
		return "error sp_section_list";
	}
	
	$sections = json_decode($result['body'], true);
	
	if(empty($sections)){
		return "error sp_section_list 2";
	}
	
	return _display_section($sections, 0);
}

function sp_section_map_shortcode_resource() {
	wp_register_script("d3js", plugins_url("d3.v7.min.js", __FILE__), array(), "1.5", false);
	wp_register_script("topojson", plugins_url("topojson.js", __FILE__), array(), "1.5", false);
	wp_register_script("sp-section-map-shortcode-jscss-script", plugins_url("map.js", __FILE__), array("d3js", "topojson"), "1.2.7", false);
	wp_register_style("sp-section-map-shortcode-jscss-style", plugins_url("map.css", __FILE__), array(), "1.2", "all");
}
add_action( 'init', 'sp_section_map_shortcode_resource' );

add_shortcode( 'sp_section_map', 'sp_section_map' );

function sp_section_map( $atts, $content, $tag ){

	wp_enqueue_script("sp-section-map-shortcode-jscss-script",array('jquery') , 1.5, true);
	wp_enqueue_style("sp-section-map-shortcode-jscss-style");

	if(!empty($atts['initial'])){
		$initial = 'data-initial="' . $atts['initial'] . '"';
	} else {
		$initial = '';
	}

	$output = '    <div class="section-map-root">';
        $output .= '      <div id="map" data-json="' . get_site_url(). '/wp-content/plugins/sp-wp-section-map/switzerland.json" ' . $initial. ' ></div>';
        $output .= '      <div class="wrapper empty"><div id="info"></div><img id="wappen" src="" /></div>';
        $output .= '      <img id="reset" src="' . get_site_url(). '/wp-content/plugins/sp-wp-section-map/ch-wappen/switzerland.svg" />';
        $output .= '    </div>';
	return $output;
}
add_filter("script_loader_tag", "add_module_to_my_script", 10, 3);

function add_module_to_my_script($tag, $handle, $src){
    if ("sp-section-map-shortcode-jscss-script" === $handle) {
        $tag = '<script type="module" src="' . esc_url($src) . '"></script>';
    }
    return $tag;
}
